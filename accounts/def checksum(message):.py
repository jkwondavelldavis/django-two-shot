def checksum(message):
    sum = 0

    for letter in message:
        int_letter = ord(letter)
        sum += int_letter
        x = sum % 26
        y = chr(x + 97)
        return y


print(checksum("bat"))
