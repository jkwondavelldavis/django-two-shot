from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptsForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required


@login_required
def Receipt_view(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt": receipt,
    }
    return render(request, "receipts/home.html", context)


@login_required
def create_view(request):
    if request.method == "POST":
        form = ReceiptsForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.purchaser = request.user
            category.save()
            return redirect("home")

    else:
        form = ReceiptsForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def categories_list(request):  # List view
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):  # List view
    accounts_list = Account.objects.filter(owner=request.user)
    context = {
        "accounts_list": accounts_list,
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):  # Create View
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            create = form.save(commit=False)
            create.owner = request.user
            create.save()
            return redirect("categories_list")

    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


@login_required
def create_account(request):  # Create View
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            new_form = form.save(commit=False)
            new_form.owner = request.user
            new_form.save()
            return redirect("account_list")

    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    print(context)
    return render(request, "accounts/create.html", context)
