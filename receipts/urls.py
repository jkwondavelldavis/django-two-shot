from django.urls import path
from receipts.views import (
    Receipt_view,
    create_view,
    categories_list,
    account_list,
    create_category,
    create_account,
)


urlpatterns = [
    path("", Receipt_view, name="home"),
    path("create/", create_view, name="create_receipt"),
    path("categories/", categories_list, name="categories_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
